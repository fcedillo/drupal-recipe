


function capitalize(string) {
  return string.charAt(0).toUpperCase() + string.slice(1);
}

function createRegExp(string) {
  return new RegExp(string, "g");
}

function replaceInHTML(rule) {
  var placeholder = rule[0];
  var value = rule[1];
  var regExp = createRegExp(placeholder);
  document.body.innerHTML = document.body.innerHTML.replace(regExp, value);
}

function htmlReplacements(ruleSet) {
  for (var i in ruleSet) {
    replaceInHTML(ruleSet[i]);
  }
}



function Nickname(text) {
  this.text = text;  
}

Nickname.prototype.getText = function() {
  return this.text;
}



function Rules() {

  this.nickname = new Nickname(_NICKNAME_);
  this.dbpass = this.generateDbpass();
  this.webpass = this.generateWebpass();
  this.sitename = capitalize(this.nickname.getText());
  this.ruleSet = [
    // placeholder, value
    [ _NAME_, _NICKNAME_ ] ,
    [ _DBPASS_, this.dbpass ] ,
    [ _WEBPASS_, this.webpass ] ,
    [ _SITENAME_, this.sitename ],
    [ _DB_SU_, '<YOUR DB SuperUser>' ],
    [ _DB_SU_PW_, 'YOUR DB SuperUser Password' ]
  ];
	
}

Rules.prototype.getRuleSet = function() {
  return this.ruleSet;
}

Rules.prototype.generateDbpass = function() {
  return this.mixStringWithSalt(this.nickname.getText());
}

Rules.prototype.generateWebpass = function() {
  return this.generateDbpass();
}

Rules.prototype.mixStringWithSalt = function(text) {
  var nickname4ch = text.substring(0, _HOW_MANY_CHARS_);
  var nicknameMixedWithSalt = "";
  for (var i = 0; i < _HOW_MANY_CHARS_; i++) {
    nicknameMixedWithSalt += nickname4ch.charAt(i) + _SALT_.charAt(i); 
  }
  return nicknameMixedWithSalt;
}

Rules.prototype.getNickname = function() {
  return this.nickname;
}



function generateRecipe() {
  var rules = new Rules();
  var ruleSet = rules.getRuleSet();
  htmlReplacements(ruleSet);
}


